# VERSION:		  0.1
# DESCRIPTION:	  Create a hello world electron app in a container
# AUTHOR:		  Nate Lusher <natelusher@docker.com>
# Derived from: Jessica Frazelle's atom launcher:
#     https://hub.docker.com/r/jess/atom/~/dockerfile/
#
# COMMENTS:
#	This file describes how to build and run the hello word electron app in a docker container
#
# Pre-requisites to run the app in a docker container on a Mac
# Install brew (ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)")
# Install cask (brew install caskroom/cask/brew-cask)
# Install socat (brew install socat)
# Install quartz (rew cask install xquartz)
# Start socat from a terminal  (socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\")
#
# USAGE:
#	Download electron-one Dockerfile:
# wget https://bitbucket.org/blanketyblanks/electron-one/raw/master/Dockerfile
#
#	# Build electron-one image
#	docker build -t electron-one .
# docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=<your-ip>:0 electron-one
#

# Base docker image
FROM node
MAINTAINER Nate Lusher <natelusher@docker.com>

# Install dependencies
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    libasound2 \
    libgconf-2-4 \
    libgnome-keyring-dev \
    libgnome-keyring-dev \
    libgtk2.0-0 \
    libnss3 \
    libxtst6 && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Install software
RUN apt-get install -y git

# Make ssh dir
RUN mkdir /root/.ssh/

###########
# Only if I make this into a private repo...
# Copy over private key, and set permissions
# where bbid is a private key in the root dir for the private bitbucket repo
#ADD bbid /root/.ssh/id_rsa
# Create known_hosts
#RUN touch /root/.ssh/known_hosts
# Add bitbuckets key
#RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# Clone the conf files into the docker container
RUN git clone git@bitbucket.org:blanketyblanks/electron-one.git

# Install the `electron` command globally
RUN npm install electron-prebuilt -g
