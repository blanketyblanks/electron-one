# README #

### electrion-one ###

This is a repo for building my first hello world electron app.


### What does this do? ###

Nothing useful, but with style.

### How do I get the code? ###
```
git clone git@bitbucket.org:blanketyblanks/electron-one.git
```

### How do I run the app? ###

#### In a docker container ####
### Pre-requisites Mac ###
```
# Install brew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
# Install cask
brew install caskroom/cask/brew-cask
# Install socat
brew install socat
# Install quartz
brew cask install xquartz
# Start socat from a terminal  
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
```
#### As a standalone app from source ####

```
# Install nodejs & npm from https://www.npmjs.com/
# Install the `electron` command globally
npm install electron-prebuilt -g
# grab a copy of the source
git clone git@bitbucket.org:blanketyblanks/electron-one.git
# from the electron-one root dir
electron .
```

###  Some links I used to figure this out ###
https://docs.docker.com/reference/builder/
https://hub.docker.com/r/jess/atom/~/dockerfile/
http://electron.atom.io/docs/v0.33.0/
https://github.com/docker/docker/issues/8710
http://stackoverflow.com/questions/23391839/clone-private-git-repo-with-dockerfile
